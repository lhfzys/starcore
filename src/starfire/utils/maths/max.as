//------------------------------------------------------------------------------
//
// 比较并返回多个数值之中的最大的值
// filename: abs.as
// author: 小兵( blog.csdn.net/aosnowasp )
// created: 2014-1-3
// copyright (c) 2013 小兵( aosnow@yeah.net )
//
//------------------------------------------------------------------------------

package starfire.utils.maths
{

	/**
	 * 比较并返回多个数值之中的最大的值
	 * @param numbers 需要比较的数值，至少两个数值（必须是数值型数据）
	 */
	public function max( ... numbers ):Number
	{
		var maxNum:Number = numbers[ 0 ];
		var lng:int = numbers.length;

		// 忽略第一位
		for( var i:int = 1; i < lng; i++ )
		{
			if( Number( numbers[ i ]) > maxNum )
				maxNum = Number( numbers[ i ]);
		}

		return maxNum;
	}
}
