//------------------------------------------------------------------------------
//
// ...
// filename: timens.as
// author: 小兵( blog.csdn.net/aosnowasp )
// created: 2014-11-4
// copyright (c) 2013 小兵( aosnow@yeah.net )
//
//------------------------------------------------------------------------------

package starfire.utils.times
{
	/**
	 * 将指定的秒数转换成中文形式的时间表示（用于计算剩余倒计时）
	 * @param second 指定的时间秒数
	 * @return 例如：2天9小时59分（分和秒不会同时显示，不足1分钟时才显示秒）
	 */
	public function timens( second:Number ):String
	{
		var days:Number = second / 86400 >> 0;
		var hour:Number = ( second % 86400 ) / 3600 >> 0;
		var minute:Number = ( second % 86400 % 3600 ) / 60 >> 0;
		var timeStr:String = "";

		if( days > 0 )
		{
			timeStr += days + "天";
		}

		if( hour > 0 )
		{
			timeStr += hour + "小时";
		}

		if( minute > 0 )
		{
			timeStr += minute + "分";
		}
		else if( second > 0 )
		{
			timeStr += second + "秒";
		}

		return timeStr;
	}
}
