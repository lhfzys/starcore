//------------------------------------------------------------------------------
//
// classname: trim
// version: 0.1.0
// author: 小兵( aosnow@yeah.net )
// created: 2013-3-22
// copyright (c) 2013 小兵( aosnow@yeah.net )
// ...
//
//------------------------------------------------------------------------------

package starfire.utils.strings
{

	/**
	 *  删除指定字符串的开头和末尾的所有空白字符（包含空格、制表符、回车符、换行符或换页符）。
	 *  @param str					- 要去掉其空格字符的字符串。
	 *  @return						- 删除了其开头和末尾空格字符的更新字符串。
	 */
	public function trim( str:String ):String
	{
		return str.replace( PATTERN_TRIM, "" );
	}
}

const PATTERN_TRIM:RegExp = /^\s+|\s+$/g;
