//------------------------------------------------------------------------------
//
// classname: jsonDecode
// version: 0.1.0
// author: 小兵( aosnow@yeah.net )
// created: 2013-3-22
// copyright (c) 2013 小兵( aosnow@yeah.net )
// ...
//
//------------------------------------------------------------------------------

package starfire.utils.strings
{
	import starfire.utils.jsons.JSONDecoder;

	/**
	 * 解码一个 JSON 字符串为本地对象
	 *
	 * @param value 表示对象的 JSON 字符串内容
	 * @param strict 该标志指示是否严格遵守 JSON 标准格式。默认值为 <code>true</code>，
	 * 若格式与 JSON 格式语法不是完全匹配，则会抛出语法错误。
	 * 设置为 <code>false</code>，以便最大程度的解码不完全正确的语法格式的 JSON 字符串
	 *
	 * @return 返回一个由 JSON 字符串解析成的本地对象
	 * <p>如果解析错误将抛出 <code>JSONParseError</code> 错误</p>
	 *
	 * @see starfire.utils.json.JSONParseError
	 */
	public function jsond( value:String, strict:Boolean = true ):*
	{
		return new JSONDecoder( value, strict ).getValue();
	}
}
