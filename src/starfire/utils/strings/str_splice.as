//------------------------------------------------------------------------------
//
// 替换字符串中，指定位置的内容
// filename: str_splice.as
// author: 小兵( blog.csdn.net/aosnowasp )
// created: 2014-5-13
// copyright (c) 2013 小兵( aosnow@yeah.net )
//
//------------------------------------------------------------------------------

package starfire.utils.strings
{

	/**
	 * 给字符串添加内容以及从中删除内容。此方法与矢量及数组的操作是类似的。
	 * @param source 需要进行操作源字符串内容
	 * @param startIndex 一个整数，它指定字符串中开始进行插入或删除的位置处的索引。
	 * 您可以用一个负整数来指定相对于结尾的位置（例如，-1 是字符串的最后一个字符）。
	 * @param deleteCount 一个整数，它指定要删除的字符数量。该数量包括 startIndex 参数中指定的元素。
	 * 如果该参数的值为 0，则不删除任何元素。
	 * @param values 将插入 startIndex 参数中的指定位置处。若值为空字符串或者 null 将不插入任何内容
	 */
	public function str_splice( source:String, startIndex:int, deleteCount:uint, values:String = null ):String
	{
		var r:String = "";
		var pos:int = startIndex < 0 ? source.length + startIndex : startIndex;
		var left:String = "";
		var right:String = "";

		pos < 0 && ( pos = 0 );
		left = source.substring( 0, pos );
		pos < source.length && ( right = source.substring( pos ));

		if( deleteCount > 0 )
		{
			deleteCount = deleteCount <= right.length ? deleteCount : right.length;
			right = right.replace( right.substr( 0, deleteCount ), "" );
		}

		r = left + values + right;

		return r;
	}
}
