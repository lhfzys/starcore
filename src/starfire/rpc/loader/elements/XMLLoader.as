//------------------------------------------------------------------------------
//
// classname: XMLLoader
// version: 0.1.0
// author: 小兵( aosnow@yeah.net )
// created: 2013-4-11
// copyright (c) 2013 小兵( aosnow@yeah.net )
// ...
//
//------------------------------------------------------------------------------

package starfire.rpc.loader.elements
{
	import starfire.rpc.loader.LoadMessage;
	import starfire.rpc.loader.LoadState;

	import flash.errors.IOError;
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.utils.ByteArray;

	[ExcludeClass]
	public class XMLLoader extends LoaderBase
	{
		protected var urlLoader:URLLoader;
		protected var decryption:Function;

		/**
		 * 创建新的XML加载器
		 * @param message 加载描述信息
		 * @param decryption 若不设置，则按简单的XML字符串加载。若设置了解压方法，则
		 * 将目标作为压缩的 BINARY 类型 XML 内容加载。XML压缩字节数组解密解压方法，
		 * 必须以 ByteArray 作为参数，并处理后返回新的 ByteArray。
		 * <p>
		 * 例如：
		 * <pre>
		 * function xmlDecryption(source:ByteArray):ByteArray
		 * {
		 * 	//... decryption xml binary
		 * 	return ByteArray;
		 * }
		 * </pre>
		 * </p>
		 *
		 */
		public function XMLLoader( message:LoadMessage, decryption:Function = null )
		{
			super( message );

			this.decryption = decryption is Function ? decryption : message.decryption;
		}

		override protected function executeLoad( message:LoadMessage ):void
		{
			var urlReq:URLRequest = new URLRequest( message.url );

			urlLoader = new URLLoader();
			urlLoader.dataFormat = decryption is Function ? URLLoaderDataFormat.BINARY : URLLoaderDataFormat.TEXT;
			toggleLoaderListeners( urlLoader, true );

			// 开始加载数据
			try
			{
				updateLoadState( LoadState.LOADING );
				urlLoader.load( urlReq );
			}
			catch( ioError:IOError )
			{
				IOErrorHandler( null, ioError.message );
			}
			catch( securityError:SecurityError )
			{
				securityErrorHandler( null, securityError.message );
			}
		}

		override protected function executeUnload( message:LoadMessage ):void
		{
			updateLoadState( LoadState.UNLOADING );
			urlLoader = null;
			updateLoadState( LoadState.UNINITIALIZED );
		}

		override protected function loadCompleteHandler( event:Event ):void
		{
			toggleLoaderListeners( urlLoader, false );

			var data:Object = urlLoader.data;

			if( decryption is Function )
			{
				var bytes:ByteArray = data as ByteArray;
				bytes = decryption( bytes );

				// 尝试读取解压后的字符串
				bytes.position = 0;
				data = bytes.readUTFBytes( bytes.length );
			}

			// 转换成XML
			data = new XML( data );

			// 文件数据保存字节数组
			try
			{
				saveCache( data );
			}
			catch( e:Error )
			{
				return;
			}

			updateLoadState( LoadState.READY );

			// 执行回调函数
			if( complete is Function )
			{
				complete( data );
			}
		}
	}
}
